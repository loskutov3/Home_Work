package services;

import DTO.Store;
import DTO.User;
import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class UserApi {

    private final String BASE_URI = "https://petstore.swagger.io/v2/";
    private final String SWAGGER_JSON = "https://petstore.swagger.io/v2/swagger.json";
    private final OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(SWAGGER_JSON);
    private RequestSpecification spec;

    public UserApi() {

        spec = given()
                .log().all()
                .baseUri(BASE_URI)
                .filter(validationFilter);
    }
    public Response createOneUser(User user) {
        return given(spec)
                .contentType(ContentType.JSON)
                .body(user)
           .when()
                .post("/user");
    }
    public Response createArrayUser(User[] users) {
        return  given(spec)
                .contentType(ContentType.JSON)
                .body(users)
            .when()
                .post("/user/createWithArray");
    }
    public Response createListUsers(ArrayList<User> users) {
        return  given(spec)
                .contentType(ContentType.JSON)
                .body(users)
           .when()
                .post("user/createWithList");
    }
    public Response getUserByName(String name) {
        return  given(spec)
                .when()
                .get("/user/" + name);
    }
    public Response updateUserByName(String name, User user) {
        return  given()
                .log().all()
                .baseUri(BASE_URI)
                .contentType(ContentType.JSON)
                .body(name)
                .when()
                .put("/user/" + name);
    }
    public Response getLoginUser(String name) {
        return  given()
                .log().all()
                .baseUri(BASE_URI)
                .when()
                .get("/user/login");
    }

    public Response getLogoutUser(String name) {
        return  given()
                .log().all()
                .baseUri(BASE_URI)
                .when()
                .get("/user/logout");
    }
    public Response deleteUser(String name) {
        return  given()
                .log().all()
                .baseUri(BASE_URI)
                .contentType(ContentType.JSON)
                .body(name)
                .when()
                .delete("/user/" + name);
    }
}

