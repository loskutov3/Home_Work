package services;

import DTO.Store;
import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class StoreApi {

    private final String BASE_URI = "https://petstore.swagger.io/v2/";
    private final String SWAGGER_JSON = "https://petstore.swagger.io/v2/swagger.json";
    private final OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(SWAGGER_JSON);
    private RequestSpecification spec;

    public StoreApi() {

        spec = given()
                .log().all()
                .baseUri(BASE_URI)
                .filter(validationFilter);
    }

    public Response createOrderPlaced(Store store) {
        return  given(spec)
                .contentType(ContentType.JSON)
                .body(store)
                .when()
                .post("/store/order");
    }
    public Response getStore(String orderId) {
        return  given(spec)
                .contentType(ContentType.JSON)
                .when()
                .get("/store/order/" + orderId);
    }
    public Response deleteStore(String orderId) {
        return  given(spec)
                .contentType(ContentType.JSON)
                .when()
                .delete("/store/order/" + orderId);
    }
    public Response getinventory() {
        return  given(spec)
                .contentType(ContentType.JSON)
                .when()
                .get("/store/inventory");
    }

}
