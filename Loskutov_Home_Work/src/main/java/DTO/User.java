package DTO;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@Setter
@Builder
@JsonSerialize
public class User {

    private String email;
    private String firstName;
    private Integer id;
    private String lastName;
    private String password;
    private String phone;
    private long userStatus;
    private String username;

}
