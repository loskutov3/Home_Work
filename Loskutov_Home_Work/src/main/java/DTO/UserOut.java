package DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@Getter
@JsonSerialize
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserOut {

    private long code;
    private String message;
    private String type;

}

