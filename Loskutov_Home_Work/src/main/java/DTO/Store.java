package DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;

@Getter
@JsonSerialize
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Store {

    public int id;
    public long petId;
    public int quantity;
    public String shipDate;
    public String status;
    public boolean complete;
}
