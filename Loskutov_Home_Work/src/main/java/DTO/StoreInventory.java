package DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@JsonSerialize
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreInventory {

    public int totvs;
    public int avail;
    public int sold;
    public int string;
    @JsonProperty("Martina")
    public int martina;
    public int avaliable;
    public int pending;
    public int available;
    @JsonProperty("not available")
    public int notAvailable;
    public int connector_up;
    public int status;
}
