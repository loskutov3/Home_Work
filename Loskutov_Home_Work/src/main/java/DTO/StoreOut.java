package DTO;

import lombok.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@Getter
@JsonSerialize
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class StoreOut {

    public int code;
    public String type;
    public String message;

}
