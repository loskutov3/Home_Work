package dataGenerator;

import DTO.Store;
import DTO.User;

import java.util.Date;

public class StoreGenerator {

    private static int id = 0;

    public static Store orderPlaced() {

        Date Data = new Date();

        return Store.builder()
                .id(0)
                .petId(0)
                .quantity(0)
                .shipDate("2022-04-29T12:41:51.577Z")
                .status("placed")
                .complete(true)
                .build();

    }
}
