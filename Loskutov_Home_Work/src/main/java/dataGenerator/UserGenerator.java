package dataGenerator;

import DTO.User;

public class UserGenerator {
    private static int id = 0;

    public static User getUser(){
        id++;
        return User.builder()
                .email("test01@test.ru")
                .userStatus(1)
                .firstName("Ivan")
                .id(id)
                .lastName("Ivanovich")
                .password("123456789")
                .phone("+7(987)712-78-40")
                .username("USER")
                .build();
    }

    public static User getExistUser(){
        return User.builder()
                .email("test2@test.ru")
                .userStatus(1)
                .firstName("Mikl")
                .id(id)
                .lastName("Semenovich")
                .password("123456789")
                .phone("+7(987)712-78-40")
                .username("USER")
                .build();

    }

}
