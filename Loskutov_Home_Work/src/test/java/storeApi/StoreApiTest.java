package storeApi;

import DTO.Store;
import DTO.StoreInventory;
import DTO.StoreOut;
import dataGenerator.StoreGenerator;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import services.StoreApi;

import java.lang.reflect.Type;
import static org.hamcrest.Matchers.lessThan;

public class StoreApiTest {

    private StoreApi storeApi = new StoreApi();

    @Test
    public void orderPlaced() {
        Store store = StoreGenerator.orderPlaced();

        Response response = storeApi.createOrderPlaced(store);

        Store actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as((Type) Store.class);

        Store expected = Store.builder()
                .id(actual.id)
                .petId(0)
                .quantity(0)
                .shipDate(actual.shipDate)
                .status("placed")
                .complete(true)
                .build();

        Assertions.assertEquals(expected, actual);
}
    @Test
    public void getStore() {

        String orderId = "6";
        Response response = storeApi.getStore(orderId);

        Store actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as((Type) Store.class);

        Store expected = Store.builder()
                .id(actual.id)
                .petId(actual.petId)
                .quantity(actual.quantity)
                .shipDate(actual.getShipDate())
                .status("placed")
                .complete(false)
                .build();

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void getStore2() {

        String orderId = "2";
        Response response = storeApi.getStore(orderId);

        StoreOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(404)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as((Type) StoreOut.class);

        StoreOut expected = StoreOut.builder()
                .code(1)
                .type("error")
                .message("Order not found")
                .build();

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void deleteStore() {

        String orderId = "3";
        Response response = storeApi.deleteStore(orderId);

        StoreOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(404)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as((Type) StoreOut.class);

        StoreOut expected = StoreOut.builder()
                .code(404)
                .type("unknown")
                .message("Order Not Found")
                .build();

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void inventory() {

        Response response = storeApi.getinventory();

        StoreInventory actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as((Type) StoreInventory.class);

        StoreInventory expected = StoreInventory.builder()
                .totvs(actual.totvs)
                .sold(actual.sold)
                .string(actual.string)
                .martina(actual.martina)
                .connector_up(actual.connector_up)
                .available(actual.available)
                .pending(actual.pending)
                .avaliable(actual.avaliable)
                .status(actual.status)
                .build();

        Assertions.assertEquals(expected, actual);
    }
}
