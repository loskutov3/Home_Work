package usersApi;

import DTO.User;
import DTO.UserOut;
import dataGenerator.UserGenerator;
import io.restassured.response.Response;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import services.UserApi;

import java.util.ArrayList;

import static org.hamcrest.Matchers.lessThan;

public class UsersApiTest {

    private UserApi userApi = new UserApi();

    @Test
    public void createValidUserTest() {
        User user = UserGenerator.getUser();

        Response response = userApi.createOneUser(user);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200)
                .message(user.getId().toString())
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void createArraUserstest() {

        User[] users = {UserGenerator.getUser(), UserGenerator.getUser()};

        Response response = userApi.createArrayUser(users);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200)
                .type("unknown")
                .message("ok")
                .build();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void createListUsersTest() {

        ArrayList<User> users = new ArrayList<User>();
        users.add(UserGenerator.getUser());
        users.add(UserGenerator.getUser());
        users.add(UserGenerator.getUser());
        users.add(UserGenerator.getUser());
        users.add(UserGenerator.getUser());

            Response response = userApi.createListUsers(users);

            UserOut actual = response.then()
                    .log().all()
                    .assertThat()
                    .statusCode(200)
                    .and()
                    .time(lessThan(3000L))
                    .extract()
                    .body()
                    .as(UserOut.class);

            UserOut expected = UserOut.builder()
                    .code(200)
                    .type("unknown")
                    .message("ok")
                    .build();

        Assertions.assertEquals(expected,actual);
        }

   @ParameterizedTest
   @ValueSource(strings= {"Test1","Test02","test3"})
    public void getUserTest(String name){

        Response response = userApi.getUserByName(name);

        UserOut actual = response.then()
                .log().all()
                .statusCode(404)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(1)
                .type("error")
                .message("User not found")
                .build();

        Assertions.assertEquals(expected,actual);

    }

    @Test
    public void updateUserTest(){
        User user = UserGenerator.getExistUser();

        Response response = userApi.updateUserByName(user.getUsername(),user);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200)
                .message(user.getId().toString())//не понятно как код формируется
                .type("unknown")
                .build();

        Assertions.assertEquals(expected,actual);
    }

    @ParameterizedTest
    @ValueSource(strings= {"01Test","02Test","03Test"})
    public void loginUser(String name){

        Response response = userApi.getLoginUser(name);

        UserOut actual = response.then()
                .log().all()
                .statusCode(200)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200)
                .type("unknown")
                .message("logged in user session:1651153732067")//не понятно как код формируется
                .build();

        Assertions.assertEquals(expected,actual);

    }
    @ParameterizedTest
    @ValueSource(strings= {"testIo","TestPo","TestOO"})
    public void logoutUserTest(String name){
        User user = UserGenerator.getExistUser();

        Response response = userApi.getLogoutUser(name);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200)
                .type("unknown")
                .message("ok")
                .build();

        Assertions.assertEquals(expected,actual);
    }
    }

